const Dao = require("./dao.js");

module.exports = class PersonDao extends Dao {
    getAll(callback) {
        super.query("select navn, alder, adresse from person", [], callback);
    }

    getOne(id, callback) {
        super.query(
            "select navn, alder, adresse from person where id=?",
            [id],
            callback
        );
    }

    createOne(json, callback) {
        var val = [json.navn, json.adresse, json.alder];
        super.query(
            "insert into person (navn,adresse,alder) values (?,?,?)",
            val,
            callback
        );
    }
    updateOne(json, callback){
        var val = [json.newname, json.adresse, json.alder,json.oldname];
        super.query(
            "Update person set navn = ?, adresse = ?, alder = ? where navn = ?",
            val,
            callback
        );
    }
    deleteOne(json, callback){
        var val = [json.navn];
        super.query(
            "delete from person where navn = ?",
            val,
            callback
        );
    }
}
/*slack testing*/